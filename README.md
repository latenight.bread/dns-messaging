# DNS Messaging
A way to send messages over DNS via spoofing IP addresses. Currently limited to local networks due to egress filtering from the ISP.
This is more of a "what if?" project than something with an actual use.

## How it works
There are 2 parts, the sender and the receiver. The sender sends a DNS query to a server but changes the source IP to the receiver's. When the DNS server answers the query, the answer is sent to the recipients IP address instead. The receiver listens on a predetermined port for DNS replies. Messages are sent by encoding information in the address of the DNS lookups, e.g. gitlab.com is an 'A', duckduckgo.com is a 'B'...

## Steps
1. A key for the meaning assigned to each domain name is predetermined, as well as the DNS resolver to be used.
2. The receiver looks up the IP address of each domain name in the key.
3. Using the key, the sender looks up domain names corresponding to the message from the DNS resolver, while spoofing the source IP to that of the receiver's.
4. The DNS resolver replies with the IP addresses of the domain names, but due to the source IP being the receiver's, the replies are sent to the receiver instead.
4. The receiver receives the IP addresses and performs a reverse DNS lookup to decode the message.

## Limitations
* I've only managed to get it to work on the local network, the most likely culprit for it not working outside is egress filtering by the ISPs, where if the source IP of a packet does not originate from inside the ISP, it gets discarded.
* Possible geographical limitations. Due to some domain names having multiple servers associated with them, the IP address may differ. Possible workaround is having the sender spoof a reverse DNS lookup instead of a normal DNS lookup or using domain names associated with one server.
* Domain names and IP addresses change over time.
* Information has to be predetermined, such as the DNS resolver to be used and the key.

## Why DNS?
* DNS can use UDP, which requires only the packet to be sent; as opposed to TCP, which requires a handshake, that's a tad difficult to do since the replies are being sent to the spoofed IP.
